#! /usr/bin/python

from protoShark.dissect import Server
from protoShark.dissect import Client
from protoShark.write import FileWriter
from protoShark.packets import Transform
from rewrite_sequence_numbers import rewrite_seq_nums
from protoShark.protocols.ipv4 import checksum as ipchecksum
from protoShark.protocols.tcp import checksum as tcpchecksum 
from protoShark.packets.utils import * 
import time, binascii

namedPipe = r'/tmp/clientServerPipe'
pcapFile = 'ipv4.pcap'
knownServerPort=443

#These are three transforms that will be run
seqTransform =Transform(rewrite_seq_nums,[is_ipv4,is_tcp])
ipchksumTransform = Transform(ipchecksum,[is_ipv4])
tcpchksumTransform = Transform(tcpchecksum,[is_ipv4,is_tcp])

#Start dissection server
serverProcess = Server.create_as_process(namedPipe,"-r %s"%pcapFile)

#Give server time to set up
time.sleep(1)

#Start client and have to collect dissected packets
c=Client(namedPipe)
pkts=[]

try:
    c.connect()
except Exception as e:
    print (e)
    sys.exit(0)
    
try:
    while(True):
        pkt=c.read_next()
        if(pkt is None):
            break            
        pkts.append(pkt)
            
except Exception as e:
    print e

#Separate tcp streams, so we can process each one independently 
tcpStreamDict={}
sort_into_tcp_sessions(pkts, tcpStreamDict)

#Create file writer that will write modified to new packets.
fw=FileWriter()
errbuf=fw.make_pcap_error_buffer()
outfile=fw.open(r'seqTransformRsts.pcap', errbuf)

#for each tcp stream change sequence numbers by some random offset
#each tcp stream will have a different offest
#then, fixup ip and tcp checksums
#write transformed packets to new pcap file
for pkt in pkts:
    
    #TRANSFORM: modify seq num
    seqTransform.run(pkt, serverPort=knownServerPort, tcpStreams=tcpStreamDict)
    
    #TRANSFORM: fixup ip checksum
    ipchksumTransform.run(pkt)
    
    #TRANSFORM: fixup tcp checksum
    tcpchksumTransform.run(pkt)
    
    #write modified packet to new pcap file
    utime, ltime = pkt.get_time()
    pktLen=pkt.get_num_bytes()
    newPktData = binascii.a2b_hex(pkt.get_pkt_data())
    fw.write(outfile,utime,ltime,pktLen,newPktData,errbuf)     

fw.close(outfile)
serverProcess.join()