#!/usr/bin/python

from protoShark.packets.utils import *
import random

'''
Rules

1. All hex values must be strings
2. All base 10 numbers must be ints
3. ALL IP addresses expressed as hex strings padded to 4 octets (len == 8) WITHOUT 0x prefix
'''

def rewrite_seq_nums(pkt, **kwargs):
    keywordCheck(['serverPort', 'tcpStreams'], **kwargs)
    serverTcpPort = kwargs['serverPort']
    tcpStreams = kwargs['tcpStreams']
    
    attrs=pkt.get_attributes()
    
    #find packet's tcp session
    streamId = int(attrs['tcp.stream'][0].get_fvalue())
    currentStream = tcpStreams[streamId]
    
    #get offset values
    clientOffset = currentStream['clientSeqnOffset']
    serverOffset = currentStream['serverSeqnOffset']
    
    #set offsets if None
    if clientOffset is None or serverOffset is None:
        
        synPkt = currentStream['pkts'][0]   #client
        synAckPkt = currentStream['pkts'][1]#server
        
        flags = int(synPkt.get_attributes()['tcp.flags'][0].get_fvalue()[2:],16)
        if 0x2 != flags:
            raise AttributeError("Syn is not first packet in tcp session: %x."%flags)    
        
        flags = int(synAckPkt.get_attributes()['tcp.flags'][0].get_fvalue()[2:],16)
        if 0x12 != flags:
            raise AttributeError("SynAck is not second packet in tcp session: %x."%flags)
        
        synSeqn = int(synPkt.get_attributes()['tcp.seq'][0].get_data(), 16)
        synAckSeqn = int(synAckPkt.get_attributes()['tcp.seq'][0].get_data(), 16)
        
        currentStream['clientSeqnOffset'] = -1*(random.randint(1, synSeqn-1))
        currentStream['serverSeqnOffset'] = -1*(random.randint(1, synAckSeqn-1))
        clientOffset=currentStream['clientSeqnOffset']
        serverOffset=currentStream['serverSeqnOffset']
        
    pktSeqn = int(pkt.get_attributes()['tcp.seq'][0].get_data(), 16)
        
    if is_server_side(pkt, serverPort=serverTcpPort):
        newSeqn = (pktSeqn + serverOffset)&0xffffffff
        pkt.set_attribute(hex(newSeqn)[2:].zfill(8),'tcp.seq')
        
        if int(pkt.get_attributes()['tcp.flags.ack'][0].get_fvalue()):
            pktAckn = int(pkt.get_attributes()['tcp.ack'][0].get_data(), 16)
            newAckn = (pktAckn + clientOffset)&0xffffffff
            pkt.set_attribute(hex(newAckn)[2:].zfill(8),'tcp.ack')
            
    else:
        newSeqn = (pktSeqn + clientOffset)&0xffffffff
        pkt.set_attribute(hex(newSeqn)[2:].zfill(8),'tcp.seq')
        
        if int(pkt.get_attributes()['tcp.flags.ack'][0].get_fvalue()):
            pktAckn = int(pkt.get_attributes()['tcp.ack'][0].get_data(), 16)
            newAckn = (pktAckn + serverOffset)&0xffffffff
            pkt.set_attribute(hex(newAckn)[2:].zfill(8),'tcp.ack')        
            
