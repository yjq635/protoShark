from protoShark.dissect import Client
from protoShark.packets import Packet
from protoShark.write import FileWriter
import binascii,sys

#connecting to server started with following command
#starting from top-level protoShark directory
#python protoShark/dissect/server.py /tmp/clientServerPipe -d tcp.port==8883,ssl -r examples/sample.pcapng

#could have read packets from wire instead of file with the following server command
#python protoShark/dissect/server.py /tmp/clientServerPipe -d tcp.port==8883,ssl -i <interface name>

if __name__ == "__main__":

    inPipe=None
    if len(sys.argv) < 2:    
        inPipe = r'/tmp/clientServerPipe'
        
    else:
        inPipe=sys.argv[1]
        
    c=Client(inPipe)
    pkts=[]
    
    try:
        c.connect()
    except Exception as e:
        print (e)
        sys.exit(0)
        
    try:
        while(True):
            pkt=c.read_next()
            if(pkt is None):
                break            
            pkts.append(pkt)
                
    except Exception as e:
        print e
    
    #get Client Hello sample packet
    print("got ssl attribute from client hello packet")
    clientHelloPkt=pkts[3]
    
    #get ssl data from packet
    pktAttrs=clientHelloPkt.get_attributes()
    
    #packet attributes are indexed by their 'abbrev' feature. This value is NOT unique. Therefore,
    #the below returns a list. You must index into the list to get the attribute(s).
    print ("got packet ssl attribute")
    sslDataList=pktAttrs['ssl']
    sslData=sslDataList[0]
    
    print ("got packet tcp attribute")
    tcpDataList=pktAttrs['tcp']
    tcpData=tcpDataList[0]
    
    #print ssl attribute
    print ("examine ssl attribute top-level.")
    print(sslData)
    print ("")
    
    #print tcp attribute
    print ("examine tcp attribute top-level.")
    print(tcpData)
    print ("")
    
    #examine ssl data
    print("walk packet tree.")
    clientHelloPkt.walk_print()
    
    #get list of ssl extensions and print each one
    print("examine ssl extensions in client hello")
    extensions=pktAttrs['ssl.handshake.ciphersuite']
    
    #each entry is its own attribute with its own set of features that can be
    #examined: abbrev, name, blurb, representation, fvalue, data, level, id, parentId
    #offset, etype, and ftype.  
    for each in extensions:
        print each.get_representation()
    
    #serialize Packet
    spkt=clientHelloPkt.serialize_to_string()
    print ("serialized packet to string of length %d")%len(spkt)
    
    #write serialized packet to file
    f=open('serializedPkt.bin','w')
    f.write(binascii.b2a_hex(spkt))
    f.close()
    
    print "Serialized packet length is", len(spkt)
    
    #unserialize Packet
    upkt=Packet.parse_from_string(spkt)
    print ("unserialized packet.")
    
    #print ssl attribute(nonrecursively) to verify success.
    ussl=upkt.get_attributes()['ssl'][0]
    print(ussl)
    
    if str(ussl) != str(sslData):
        print ("Error: parsed ssl not same as pre-serialized ssl.")
    
    else:
        print("Serialize-Parse succeeded.")
    
    
    #modify each packet by changing port from 8883 to 1111
    #we don't fix tcp checksum in this demo. Should do this in real application.
    #write modified packets to new pcap file
    fw=FileWriter()
    errbuf=fw.make_pcap_error_buffer()
    outfile=fw.open(r'modifiedSample.pcap', errbuf)
    
    for each in pkts:
        src=each.get_attributes()['tcp.srcport'][0]
        dst=each.get_attributes()['tcp.dstport'][0]
        
        #this is 1111 represented as hex string
        newHexString='0457'
        
        if src.get_fvalue() == '8883':
            each.set_attribute(newHexString,'tcp.srcport')
        
        elif dst.get_fvalue() == '8883':
            each.set_attribute(newHexString,'tcp.dstport')
        
        utime, ltime = each.get_time()
        pktLen=each.get_num_bytes()
        newPktData = binascii.a2b_hex(each.get_pkt_data())
        fw.write(outfile,utime,ltime,pktLen,newPktData,errbuf)            
    
    fw.close(outfile)
        
    	
