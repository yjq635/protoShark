# ProtoShark HOW-TO

Packet modification/creation and subsequent (re)writing is the main motivation for this project. The easiest way to create a packet or set of related packets (e.g., TCP session) is to use existing packets as templates. Modify those existing packets according to need and then write them out to a pcap file or network interface.

## Packet Reading, Dissecting, Modifying, and (re)Writing

ProtoShark uses a custom implementation of tshark for packet reading and dissecting. The tshark code is compiled as a shared library and exposed via the protoShark dissect module. Packet data can be ingested from either packet capture files or from network interfaces. 

ProtoShark provides the dissected packets as native Python objects that can be examined, modified, and written out to either other packet capture file(s) or network interface(s). The protoShark module wraps libpcap and uses its functionality for packet writing and for packet capture file creation. 

The dissect.Server class reads in packet data, dissects them, and writes out the serialized results via a named pipe. The dissect.Client class is the consumer of server data. The dissect client connects to a dissect server's named pipe and reads serialized packets one at a time. On the server side, protoShark leverages (C++) Google Protocol Buffers to serialize packet dissection data before writing it to its output named pipe. On the client side, protoShark leverages (Python) Google Protocol Buffers to parse the serialized packet dissection data after reading it from the server's named pipe. The dissect.Client class then uses the packet dissection data to construct a native Python representation of the server dissected packets. 

The WireWriter and FileWriter classes write data to network interface(s) and pcap file(s), respectively. These classes wrap libpcap functionality making packet writing easier and more intuitive. 

## The write.WireWriter class
### `WireWriter()`: Overview

The protoShark write.WireWriter class makes writing to network interfaces easy. It is typically used to write modified packet data. However, it is designed to write arbitrary binary data to one or more network interfaces. 

### `WireWriter()`: API<br>
`WireWriter()`: Constructor<br>
* Arguments: None

`WireWriter.open_interfaces_for_sending(iface_list)`<br>
* Arguments
	* iface_list: python list of interface names identifying interfaces that writer object will write to.<br>
* Returns: None
	* raises RuntimeError Exception if interface(s) cannot be opened.
	
`WireWriter.close_sending_interfaces(iface_list=[])`<br>
* Arguments
	* iface_list: python list of interface names identifying interfaces that writer object will close. By default, will close all interfaces opened by writer object.<br>
* Returns: None

`WireWriter.write(bufr, stripLastByte=False)`<br>
* Arguments:
	* bufr: Binary data buffer to write.<br>
	* stripLastByte: If True, doesn't write last byte in buffer. Use if writing a Python binary string that is NULL terminated. Default is False.<br>
* Returns: Number of bytes written.

### *`WireWriter()`: Example Code*<br>

```python<br>

#write arbitrary data to wire

if __name__=='__main__':


    if 3 > len(sys.argv):
        print("usage: %s data interfaceName [interfaceName(s)]" % sys.argv[0])
        sys.exit(0)
        
    #These interfaces will be written to
    interfaces=[]
    for each in sys.argv[2:]:
        interfaces.append(each)
    
    data=sys.argv[1]
        
    ww=WireWriter()   
    ww.open_interfaces_for_sending(interfaces)
    for each in ww.send_interfaces_list:
        try:
            numBytesWritten=ww.write(data, stripLastByte=True)
            print "wrote %d bytes"%numBytesWritten
        except Exception as e:
            print e
    
    #by default will close all opened interfaces
    ww.close_sending_interfaces()
 ```
 
 ```python<br>
 
 #write packet to wire
 
...
ww=WireWriter()   
ww.open_interfaces_for_sending(interfaces)
pktData=binascii.a2b_hex(pkt.get_pkt_data())
 
try:
    numBytesWritten=ww.write(pktData)
    print "wrote %d bytes"%numBytesWritten
except Exception as e:
    print e

ww.close_sending_interfaces()
 
 ```
 
## The write.FileWriter class<br>
### `FileWriter()`: Overview<br>
The FileWriter class is used to write packets to a pcap file. 

`FileWriter()`: Constructor<br>
* Arguments: None

`FileWriter.open(outFilePath)`<br>
* Arguments:
	* outFilePath: path to output pcap file, the file being written to.<br>
* Returns: handle to output file on success, None on failure.
* This function creates the output pcap file and returns a handle to that file.

`FileWriter.close(outFilePath)`<br>
* Arguments:
	* outFilePath: path to output pcap file, the file being written to.<br>
* Returns: None
* Flushes all buffers to file and closes file.

`FileWriter.make_pcap_error_buffer()`<br>
* Arguments: None
* Returns: Error message buffer for libpcap error messages.
* Required for write.

`FileWriter.write(outFilePath, packetTimeSec, packetTimeFractional, nBytes, bData, errorBuf)`<br>
* Arguments
	* outFilePath: path to output pcap file, the file being written to.<br>
	* packetTimeSec: packet epoch time seconds. get from existing protoShark packet: `sectime, fractime = pkt.get_time()`.<br>
	* packetTimeFractional: packet epoch time fractional second. get from existing protoShark packet: `sectime, fractime = pkt.get_time()`.<br>
	* nBytes: number of bytes to write.<br>
	* bData: bytes to write (binary data).<br>
	* errorBuf: libpcap error buffer created using FileWriter.make_pcap_error_buffer().<br>
* Returns
	* number of bytes written on success, None on failure.<br>
	* on failure raises RuntimeError Exception.<br>
* Writes binary packet data to pcap file. Refer to below example use for important details.

### *`FileWriter()`: Example Code*<br>

```python<br>

    from protoShark.dissect import Client

    #client acquires packets from dissect.Server via named pipe
    inPipe = r'/tmp/clientServerPipe'
    c=Client(inPipe)
    
    #open new pcap file that we will write modified packets to
    fw=FileWriter()
    errbuf=fw.make_pcap_error_buffer()
    outfile=fw.open(r'/home/me/test_output_file.pcap', errbuf)

    #connect to dissect.Server named pipe
    try:
        c.connect()
    except Exception as e:
        print (e)
        sys.exit(0)
    
    #read packets from dissect.Server one at a time
    #modify packet and write modified packet to new pcap file
    try:
        for _ in xrange(10):
            pkt=c.read_next()
            if(pkt is None):
                break            

            pkt.set_attribute('000000000000', 'eth.src', mustExist=True)
            pkt.set_attribute('111111111111', 'eth.dst', mustExist=True)
            
            utime, ltime = pkt.get_time()
            dataLen=pkt.get_num_bytes()
            
            newPktData = binascii.a2b_hex(pkt.get_pkt_data())
            fw.write(outfile,utime,ltime,dataLen,newPktData,errbuf)            

            #pkt.walk_print()
            
    except Exception as e:
        print e
    
    fw.close(outfile)
```
  
## The dissect.Server Class
### `Server(pipeName, commandLine)`: Overview

The protoShark dissect.Server class provides two mechanisms for creating server objects. The best approach is to use the `Server.create_as_process()` function. It creates a new server in its own process and returns the process handle to the caller. The other approach is to directly use the Server class API to create a dissection server in the main process.

*Note: Only ONE dissection server can run in a given process.*

### `Server(pipeName, commandLine)`: API<br>
`Server(pipeName, commandLine)`: Constructor<br>
* Arguements
	* pipeName: name/path to use for output named pipe.<br>
	* commandLine: tshark style commandline.<br>

`Server.start()`:<br>
* Arguments: None.
* Returns: None.
* Creates output named pipe.
* Starts dissection server.
* Will block until Server.Client reads all server dissected packets.

#### *`Server(pipeName, commandLine)`: Example Code* <br>
```python<br>
#!/usr/bin/python

from protoShark.dissect import Server

namedPipe = r'/tmp/clientServerPipe'
pcapFile = '/protoShark/examples/ipv4ssl.pcap'

#The `s.start()` call will block until server has processed all packets and provided them to the client
s = Server(namedPipe,"-d tcp.port==8883,ssl -r %s"%pcapFile)
s.start()
```

@staticmethod<br>
`Server.create_as_process(pipeName, commandline)` <br>
* Arguments
	* pipeName: name/path to use for output named pipe.<br>
	* commandLine: tshark style commandline.<br>
* Returns: handle to new server process.
* Spawns server in its own process.
* New process will write dissected packets to output named pipe.
* This call will NOT block.

#### *`Server.create_as_process(pipeName, commandline)`: Example Code*<br>
```python<br>
#!/usr/bin/python

from protoShark.dissect import Server

namedPipe = r'/tmp/clientServerPipe'
pcapFile = '/protoShark/examples/ipv4ssl.pcap'

#This function call will not block
serverProcess = Server.create_as_process(namedPipe,"-d tcp.port==8883,ssl -r %s"%pcapFile)

#...do other stuff here

#Program will not terminate until server process has terminated
serverProcess.join()
```
## The dissect.Client Class
### `Client(pipeName)`: Overview

The protoShark.Client class has only three jobs. In order of use, they are:
1. read packet dissection data from dissect servers after connecting to server named pipe, 
2. create a Python representation of dissected packets, 
3. and deliver the Python packet objects to caller.

### `Client(pipeName)`: API<br>
`Client(pipeName)`: Constructor
* Arguments
	* pipeName: Name of dissect server pipe that client will connect to.

`Client.connect()`<br>
* Arguments: None.
* Returns: None.
* Connects client to server named pipe.

`Client.read_next()`
* Arguments: None.
* Returns:
	* ProtoShark.Packet object if successful.
	* None if EOF or error.
* Reads next serialized packet from dissect server.

`Client.disconnect()`
* Arguments: None.
* Returns: None.
* Disconnects client from server and closes client side of named pipe.

### *`Client(pipeName)`: Example Code*<br>
```python
#!/usr/bin/python

from protoShark.dissect import Client

#This is the named pipe created by the dissect server
namedPipe = r'/tmp/clientServerPipe'

#Create client and then connect it to server
c = Client(namedPipe)
c.connect()

pktList=[]

while(True):
    
    #get next packet
    pkt=c.read_next()
    
    #Client.read_next() returns None at EOF
    if(pkt is None):
        break            
    
    #save packet for use at a later time
    pktList.append(pkt)
```

## The packets.Packet Class
### `Packet()`: Overview

The protoShark Packet class is a native python representation of a dissected packet. Under the hood, protoShark Packets are created by walking Wireshark's epan dissect tree. Given this, it's best to think of a protoShark Packet similarly: as a tree structure (although, it's not actually implemented as such). The protoShark Packet is a collection of Packet.Attribute objects. Logically, the attributes create a tree structure. The Packet's 'frame' attribute is the tree root. All other Packet attributes are descendants of 'frame'. So, for example, all top-level protocols such as 'eth', 'ip, 'tcp', 'ssl', etc. are children of 'frame'. Moreover, each of these top-level protocol attributes are themselves logically organized as subtrees under 'frame'. For example, the 'eth' attribute children include 'eth.src' and 'eth.dst'. One can view a protoShark Packet's logical organization by calling `Packet.walk_print()`.

*Note: the protoShark Packet logical tree structure is a bit different from the Wireshark epan dissect tree logical structure. In Wireshark, the frame node is treated as a top-level protocol. Hence, frame and all other top-level protocols such as eth, ip, and tcp are at the same level. They are all children of packet root node.*

### `Packet()`: API
`Packet()`: The Packet class does not have a public constructor. Packet objects are created and provided to caller from dissect.Client objects only.

`Packet.get_root()`<br>
* Arguments: None.
* Returns: Reference to packet root attribute, the Packet's frame attribute.

`Packet.get_attributes()`<br>
* Arguments: None.
* Returns: Reference to Packet Attribute dictionary.
	* The Packet Attribute dictionary holds all Packet Attributes referenced by their abbrev feature. <b>The Packet Attribute abbrev feature is not necessarily unique within the scope of a protoShark Packet. Therefore, each key indexes a Python list of matching Packet Attributes</b>.

`Packet.walk_print(node=None)`
* Arguments: (optional) Start node. By default starts at Packet root, its frame attribute.
* Returns: None.
* Walks Packet tree using node as tree root (frame by default) and prints each Packet attribute.
* Used to see logical layout of Packet.

`Packet.set_attribute(attrVal, attrName, enforceConsistant=True, mustExist=True)`
* Arguments
	* attrVal: New value that attribute will be set to. <b>Must be hex string representation of value</b>.<br>
	* attrName: Name of attribute being changes, it's abbrev feature, e.g. 'eth.src'<br>
	* enforceConsistant: By default, `Packet.set_attribute()` runs a consistency check before setting new attribute values. 
	    * It verifies and requires that the new value be the same length as the original value.<br>
	    * It compares local attribute data to the attribute's data in the Packet's raw data. It requires that the two be equal. Common reasons for this check to fail are as follows.<br>
		    * Attribute value has already been changed once.<br>
		    * The target attribute is something that Wireshark derives from Packet data and is not part of raw Packet data.<br>
		    * The target attribute is part of a larger attribute and Wireshark doesn't provide the correct offset. TCP flags are an example of this. When changing one TCP flag, one must change the entire 16-bit value of the 'tcp.flags' attribute. Attempts to change/unset only one flag (e.g., tcp.flags.syn) will fail because Wireshark provides the same 'tcp.flags' offset for all TCP flags: tcp.flags.res, tcp.flags.ns, tcp.flags.cwr, tcp.flags.syn, tcp.flags.ack, etc.<br>
	    * Set enforceConsistant to False in `Packet.set_attribute()` call in the following two circumstances.<br>
		    * The new value for the target attribute must be a different length from the original value. <b>Note: you will become responsible for tracking and maintaining packet protocol boundaries.</b><br>
		    * For some reason, the same attribute must have its value changed multiple times.<br> 
	* `mustExist`: Set to True by default. If True, requires that the attribute being set exists in the target Packet. If set to False, `Packet.set_attribute()` will silently fail if the attribute is missing.
* Returns: None.
* This function ONLY modifies the target attribute's and related attribute's data feature as well as the raw Packet data. Attribute features such as 'representation' and 'blurb' remain set to their original values. Changing these other feature would require that modified Packets be re-dissected. (re)Writing modified Packets to a pcap file or network interface requires ONLY the Packet's raw data. Hence re-dissecting modified Packets would create a lot of overhead for little return.

#### *`Packet.set_attribute(attrVal, attrName, enforceConsistant=True, mustExist=True)`: Example Code*<br>

```python<br>
c = Client(namedPipe)
c.connect()

pktList=[]

while(True):
    
    #get next packet
    pkt=c.read_next()
    
    #Client.read_next() returns None at EOF
    if(pkt is None):
        break            
    
    #Modify each packet such that it's source ethernet address is set to 11:11:11:11:11:11
    pkt.set_attribute('111111111111','eth.src')
    
    #save packet for use at a later time
    pktList.append(pkt)
```

`Packet.set_raw(value, startHexOffset, overWrite=True)`: TO-DO
* Arguments
    * value: New value expressed as hex string.<br>
    * startHexOffset: Offset in raw data hex representation where new value starts.<br>
    * overWrite: Set to True by default.<br>
        * If True, over-write existing data in raw Packet data hex string starting at insertion point.<br>
        * If False, insert new data starting at insertion point and push existing data down toward end of Packet data hex string.<br>
* Returns: None.
* Modifies Packet data using data hex-offset in Packet instead of Packet.Attribute name.

`Packet.regex_replace_raw(value, matchRegex, startingAtHexOffset=0)`: TO-DO
* Arguments
    * value: New value expressed as hex string.<br>
    * matchRegex: The FIRST hex string value that matches this regular expression will be replaced by value.<br>
    * startingAtHexOffset: Matching will start of 'startingAtHexOffset' in Packet raw data hex string.<br>
* Returns: None
* Modifies Packet data using regular expression instead of Packet.Attribute name.

`Packet.get_time()`
* Arguments: None.
* Returns: tuple, (upper, lower).
	* upper: seconds.<br>
	* lower: fractional second.<br>
* Returns Packet time-stamp. This information is required to packet (re)writing.

`Packet.get_num_bytes()`
* Arguments: None.
* Returns: Number of bytes in Packet raw data.

`Packet.get_pkt_data()`
* Arguments: None.
* Returns: Hex string representation of Packet raw data.
* Equivalent to: Packet.get_attributes()[frame.data][0].get_data()

`Packet.serialize_to_string()`
* Arguments: None.
* Returns: Google Protocol Buffers serialized version of Packet as a binary Python string.
* Allows one to save dissected Packet as a binary blob.


@staticmethod<br>
`Packet.parse_from_string(serializedPacketString)`:
* Arguments
	* serializedPacketString: Binary serialized string created using Packet.serialize_to_string().<br>
* Returns: protoShark.Packet Python object

## The packets.Packet.Attribute Class
### `Packet.Attribute()` Overview
Packet.Attribute is a subclass of the Packet Class. It represents one attribute (tree node) in a protoShark Packet. The Packet.Attribute class provides a set of feature elements that describe the attribute data in different ways. As is true with the Packet class, it doesn't have a public constructor. Packet.Attribute objects are only created by dissect.Child objects. Below is a string representation of a Packet.Attribute object.<br>
*Packet Attribute at a glance*<br>
```bash<br>
Attribute:
     abbrev         		eth.src
     name           		Source
     blurb          		Source Hardware Address
     fvalue         		bc:5f:f4:45:78:7b
     level          		1
     offset         		6
     ftype          		26
     ftype_desc     		FT_ETHER
     representation 		Source: AsrockIn_45:78:7b (bc:5f:f4:45:78:7b)
     data           		bc5ff445787b

Number of children: 5
 eth.src_resolved
 eth.addr
 eth.addr_resolved
 eth.lg
 eth.ig
```

### `Packet.Attribute()` API
`Packet.Attribute()`: This class does not have a public constructor. It is attribute objects are created by dissect.Child objects

`Packet.Attribute.get_children()`<br>
* Arguments: None.
* Returns: Python list containing the Attribute's immediate child Attributes.

`Packet.Attribute.get_abbrev()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'abbrev' feature.

`Packet.Attribute.get_name()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'name' feature.

`Packet.Attribute.get_blurb()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'blurb' feature.

`Packet.Attribute.get_representation()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'representation' feature.

`Packet.Attribute.get_fvalue()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'fvalue' feature.

`Packet.Attribute.get_data()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'data' feature.
* It is a hex string representation of the attributes raw bytes.

`Packet.Attribute.get_level()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'level' feature.
* A Packet's 'frame' attribute is its root. Hence, it has level == 0.
* All top-level Packet attributes such as 'eth', 'ip', 'tcp', 'ssl' are level 1.
* In general, a given node's level value is one more than its parent's level.

`Packet.Attribute.get_id()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'id' feature.
* Every attribute has an integer id value that is unique among all Packet attributes within a given capture file or capture session.

`Packet.Attribute.get_parentId()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'parentId' feature.

`Packet.Attribute.get_offset()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'offset' feature.
* Value is start index of Attribute data first **byte** in parent Packet's **binary** data blob.

`Packet.Attribute.get_hex_offset()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'offset' feature.
* Value is start index of Attribute data first **hex character** in **hex string** representing parent Packet's data.
* 2*offset == hex_offset.

`Packet.Attribute.get_ftype()`:<br>
* Arguments: None.
* Returns: Value of the Attribute's 'ftype' feature.
* ftype is 'Field Type' value as defined by Wireshark.


## The packets.Transform Class
### `Transform(transformFunction, apply-if-true-conditionList)`: Overview

Packet modification tasks typically involve multiple Packets and multiple data transformations. The Transform Class is a light-weight convienence class that allows one to apply transformations to packets efficiently. The transforms can be applied either conditionally or unconditionally. Consider the below code snippet taken from *protoShark/examples/transformationExample.py*.

```python<br>
...

from rewrite_sequence_numbers import rewrite_seq_nums
from protoShark.protocols.ipv4 import checksum as ipchecksum
from protoShark.protocols.tcp import checksum as tcpchecksum 

...

#These are three transforms that will be run
seqTransform =Transform(rewrite_seq_nums,[is_ipv4,is_tcp])    #per tcp session modify tcp seq nums by some offset
ipchksumTransform = Transform(ipchecksum,[is_ipv4])           #per packet fixup ipchecksum
tcpchksumTransform = Transform(tcpchecksum,[is_ipv4,is_tcp])  #per packet fixup tcpchecksum

#Separate tcp streams, so we can process each one independently 
tcpStreamDict={}
sort_into_tcp_sessions(pktList, tcpStreamDict)

for pkt in pktList:
    
    #TRANSFORM: modify seq num
    seqTransform.run(pkt, serverPort=knownServerPort, tcpStreams=tcpStreamDict)
    
    #TRANSFORM: fixup ip checksum
    ipchksumTransform.run(pkt)
    
    #TRANSFORM: fixup tcp checksum
    tcpchksumTransform.run(pkt)
```
The above code snippet defines three transforms that will be applied in series to each packet in pktList.<br> 
* The 'seqTransform' and 'tcpchksumTransform' will only be applied to packets that are both ipv4 and tcp. 
* 'ipchksumTransform' will be applied to all ipv4 packets. 

### `Transform(transformFunction, apply-if-true-conditionList)`: API<br>
`Transform(runner, conditionalFunctList=[])`: Constructor<br>
* Arguements
    * runner: Function that Transform calls to modify Packet data.<br>
        * Prototype: `runner(pkt, **kwargs)`<br>
        * Arguments: <br>
            * pkt: Packet to be transformed.<br>
            * kwargs: Transform function input parameters defined as `runner(pkt, name1=value1, name2=value2, ..., nameN=valueN)`<br>
        * Returns: None<br>
        * Must silently ignore any input parameters that it might receive but does not use.<br>
    * conditionalFunctList: A list of functions that Transform executes determine if Transform should be run against a given Packet.<br>
        * Prototype: `condFunct(pkt, **kwargs)`<br>
            * pkt: Packet to be might/might not be transformed.<br>
            * kwargs: conditional function input parameters defined as `condFunct(pkt, name1=value1, name2=value2, ..., nameN=valueN)`<br>
        * Returns: Must return True if Transform should run against given Packet. Must return False if Transform should NOT run against given Packet.<br>
        * Must silently ignore any input parameters that it might receive but does not use.<br>

`Transform.run(pkt, **kwargs)`<br>
* Arguments:<br>
    * pkt: Packet to be transformed.<br>
    * kwargs: Transform function input parameters defined as `run(pkt, name1=value1, name2=value2, ..., nameN=valueN)`<br>
* Returns: None<br>
* Runs series of conditional functions defined for transform. If all return True, executes Transform runner (transformation code).<br>
* Will provide same 'pkt' and 'kwargs' arguments to both conditional function and runner function.<br>

## packets.util
Collection of Packet related utility functions.

`keywordCheck(keyWordList, **kwargs)`:<br>
* Arguments:
	* keyWordList: list of keywords.<br>
	* kwargs: dictionary of function input parameters.<br>
* Returns: None if every keyword in keyWordList exists in kwargs. Otherwise, raises AttributeError exception.

`is_ipv4(pkt, **kwargs)`:<br>
* Arguments:
	* pkt: protoShark Packet object.<br>
	* kwargs: dictionary of function input parameters.<br>
* Returns: True if Packet contains ipv4 protocol. Otherwise, returns False.

`is_tcp(pkt, **kwargs)`:<br>
* Arguments:
	* pkt: protoShark Packet object.<br>
	* kwargs: dictionary of function input parameters.<br>
* Returns: True if Packet contains tcp protocol. Otherwise, returns False.

`is_server_side(pkt, **kwargs)`:<br>
* Arguments:
	* pkt: protoShark Packet object.<br>
	* kwargs: dictionary of function input parameters.<br>
		* Must provide input parameter 'serverPort' that specifies the tcp port that server listens on.<br>
* Returns: True if Packet contains tcp.srcport and equals 'serverPort'. Otherwise, returns False.

`is_client_side(pkt, **kwargs)`:<br>
* Arguments:
	* pkt: protoShark Packet object.<br>
	* kwargs: dictionary of function input parameters.<br>
		* Must provide input parameter 'serverPort' that specifies the tcp port that server listens on.<br>
* Returns: True if Packet contains tcp.srcport and it does NOT equal 'serverPort'. Otherwise, returns False.

`p_select(pA)`:<br>
* Arguments:
	* pA: Floating point number greater than 0 and less than or equal to 1.<br>
* Returns:
	* Function randomly chooses floating pointer number 'p' in range (0,1].<br>
	* Returns True if 'p' is less than or equal to pA. Otherwise, returns False.<br>
	
`interleave_pkts(pktListA, pktListB, p=0.5, startA=True, minTimeVariance = .00000000001, maxTimeVariance = .00000000075)`:<br>
* Arguments:
	* pktListA: Python list of protoShark Packet objects.<br>
	* pktListB: Python list of protoShark Packet objects.<br>
	* p: Floating point number greater than 0 and less than or equal to 1.<br>
	* startA: When interleaving pktListA and pktListB start with pktListA if startA is True. Otherwise, start with pktListB.<br>
	* minTimeVariance: Min percent timestamp difference between Packet n and Packet n+1.<br>
	* maxTimeVariance: Max percent timestamp difference between Packet n and Packet n+1.<br>
* Returns:
	* Python list containing all Packets from both pktListA and pktListB interleaved<br>
	* Function will write packet from current list with probability 'p'. Function will switch lists with probability 1-'p' and then write packet. This other list will then become the current list.<br>
	
`hex2dd(hexip)`:<br>
* Arguments:
	* hexip: Hex string representation of ipv4 ip address.<br>
* Returns:
	* Dotted-decimal representation of input hex ip address.<br>
	
`sort_into_tcp_sessions(pktList,sessionDict)`:<br>
* Arguments:
	* pktList: Python list of protoShark Packet objects.<br>
	* sessionDict: Empty Python dictionary object.<br>
* Returns: None.
* Function separates all packets in pktList by 'tcp.stream' attribute.
* Dictionary will have form of ...

```python<br>
  sessionDict[tcpStream]={}
  sessionDict[tcpStream]['pkts']=[]
  sessionDict[tcpStream]['clientIp']=None   
  sessionDict[tcpStream]['clientPort']=None   
  sessionDict[tcpStream]['clientSeqnOffset']=None
  sessionDict[tcpStream]['serverSeqnOffset']=None
```
* 'tcpStream' key is an integer representing Packet attribute value for 'tcp.stream'
* Function sets 'tcpStream' and adds tcp stream packets to `sessionDict[tcpStream]['pkts']` list.
* Function does not set 'clientIp', 'clientPort', 'clientSeqnOffset', and 'serverSeqnOffset'. These are reserved for other  functions.

## Protocols
Collection of protocol specific utility functions.

### protocols.ipv4
`ipv4.checksum(pkt)`:<br>
* Arguments: protoShark Packet object
* Returns: None
* Computes Packet ipv4 checksum and sets Packet ip.checksum attribute value

### protocols.tcp
`tcp.checksum(pkt)`:<br>
* Arguments: protoShark Packet object
* Returns: None
* Computes Packet tcp checksum and sets Packet tcp.checksum attribute value




