#!/usr/bin/python

###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################

from distutils.core import setup, Extension
import os
import site, commands
import sys

def pkgconf(*packages, **kw):
    cfgInfo = {'-I':'include_dirs', '-L': 'library_dirs', '-l': 'libraries'}
    for fld in commands.getoutput("pkg-config --libs --cflags %s" % ' '.join(packages)).split():
        kw.setdefault(cfgInfo.get(fld[:2]), []).append(fld[2:])
    return kw

glib_includes=pkgconf('glib-2.0')['include_dirs']

cdir=os.getcwd()
install_dir='protoShark'

dissect_module = Extension('protoShark_dissect',
                    sources=['protoShark/dissect/c_src/cfile.c',
                             'protoShark/dissect/c_src/frame_tvbuff.c',
                             'protoShark/dissect/c_src/protoSharkPrint.c',
                             'protoShark/dissect/c_src/protoShark.c',
                             'protoShark/dissect/c_src/capture_opts.c',
                             'protoShark/dissect/c_src/protoShark_capture_sync.c',
                             'protoShark/dissect/c_src/capture-pcap-util.c',
                             'protoShark/dissect/c_src/capture_ui_utils.c',
                             'protoShark/dissect/c_src/extcap.c',
                             'protoShark/dissect/c_src/extcap_parser.c',
                             'protoShark/dissect/c_src/capture_ifinfo.c',
                             'protoShark/dissect/c_src/capture-pcap-util-unix.c',
                             'protoShark/dissect/c_src/util.c',
                             'protoShark/dissect/c_src/sync_pipe_write.c',
                             'protoShark/dissect/c_src/protoShark.pb.cc',
                             'protoShark/dissect/c_src/exportAttribute.cc'],
                    library_dirs=[],
                    libraries= ['wireshark',
                               'wsutil',
                               'wiretap',
                               'glib-2.0',
                               'pcap',
                               'pthread',
                               'protobuf'],
                    include_dirs=['protoShark/dissect/c_src',
                                  'protoShark/dissect/c_src/wireshark-2.0.4',
                                  'protoShark/dissect/c_src/wireshark-2.0.4/epan',
                                  'protoShark/dissect/c_src/wireshark-2.0.4/wsutil',
                                  'protoShark/dissect/c_src/wireshark-2.0.4/wiretap',
                                  'protoShark/dissect/c_src/wireshark-2.0.4/capchild',
                                   glib_includes[0],
                                   glib_includes[1]],
                    extra_compile_args=['-std=c++11'])

write_module = Extension('protoShark_write',
               sources=['protoShark/write/c_src/write.c'],
               libraries=['pcap'],
               include_dirs=['protoShark/write/c_src'])

def package_files(directory, omit=None):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        if omit is not None and 0 < path.find(omit):
            continue
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
            
    return paths

setupfiles = package_files('protoShark', 'c_src')

setup(name='protoShark',
      version='0.1Beta',
      description='Scripted packet inspecting, modification, rewriting.',
      ext_modules=[dissect_module,write_module],
      author='Mark Landriscina',
      packages=['protoShark', 'protoShark.dissect', 'protoShark.write'],
      package_data={'protoShark':setupfiles},
      py_modules=[])


