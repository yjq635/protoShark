# Version
BETA 0.1-dev<br>
Being created in support of NSA's 2017 Codebreaker Challenge which is scheduled to launch Sept 15, 2017.<br>

# Description and Motivation
Python extension for scriptable network data collection, deep packet inspection, packet modification, packet writing to network interface or pcap file. Packets provided to user as native python objects. Incorporates custom tshark implementation for packet reading and dissection, leverages Google protocol buffers for packet serializing and parsing. Packet (re)writing uses libpcap. Dockerfile provided that creates Ubuntu 16.04 Linux container with protoShark installed. Some example code provided. Building directly from source is a bit involved but instructions provided. Only builds/runs on Linux. Windows use is best via Docker.

Packet modification/creation and subsequent (re)writing is the main motivation for this project. The easiest way to create a packet or set of related packets (e.g., TCP session) is to use existing packets as templates. Modify those existing packets according to need and then write them out to a pcap file or network interface. The Wireshark project provides the broadest set of packet dissectors available anywhere, which is why a customized tshark was chosen for packet reading/dissection. It is compiled as a shared lib and incorporated into the protoShark python module.<br>

# VM INSTALL
See protoSharkDockerFiles/Dockerfile and protoSharkDockerFiles/DockerREADME.md

OR ...

Starting with clean Ubuntu 16.04 Linux host and starting in / dir and running as root. Modify according to personal preference.
```bash

apt-get -y update
apt-get -y install man
apt-get install -y apt
apt-get install -y apt-utils
apt-get install -y git
apt-get install -y libpcap-dev
apt-get install -y libglib2.0-dev
apt-get install -y libpython-dev
apt-get install -y autoconf automake libtool curl make g++ unzip
apt-get install -y vim
apt-get install -y python-setuptools
apt-get -y install net-tools
apt-get -y install ssh
apt-get -y install iputils-ping
apt-get -y install dos2unix
printf "Package: libwireshark-dev\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/libwiresharkdev
printf "Package: libwsutil-dev\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/libwsutildev
printf "Package: libwiretap-dev\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/libwiretapdev
printf "Package: wireshark-common\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/wiresharkcommon
printf "Package: libwireshark-data\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/libwiresharkdata
printf "Package: tshark\nPin: version 2.0.2+ga16e22e-1\nPin-Priority: 600" >/etc/apt/preferences.d/tshark
cd / && git clone https://github.com/google/protobuf && cd /protobuf && git checkout tags/v3.4.0
cd /protobuf && ./autogen.sh && ./configure --prefix=/usr && make && make install && ldconfig
cd /protobuf/python && python ./setup.py build && python ./setup.py install
apt-get -y install sudo && \
    useradd -d /home/user -ms /bin/bash user && \
    printf "user ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && chmod 0440 /etc/sudoers.d/user && \
    echo "wireshark-common wireshark-common/install-setuid boolean true" | debconf-set-selections -v && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install libwireshark-dev libwsutil-dev libwiretap-dev wireshark-common tshark && \
    usermod -aG wireshark user

cd / && git clone https://gitlab.com/MLandriscina/protoShark.git
cd protoShark && python ./setup.py build && python ./setup.py install
su user
```

# ProtoShark API: See HOW-TO.md

# Some Sample Code: Dissect Server and Client

*Create server to read pcap file, parse packets, and send packet data to dissect Client.*<br>
```python
from protoShark.dissect import Server
import sys

if __name__ == "__main__":

    if(3 > len(sys.argv)):
        print("usage: %s <outpipename> [options] [infilename]" % sys.argv[0])
        sys.exit(0)

    s=None
    options=' '.join(sys.argv[2:])
    pname=str(sys.argv[1])

    try:
        s = Server(pname, options)
        if s is not None:
            s.start()
    except Exception as (e):
        print(e)
```

*Run dissect Server and Treat all TCP 8883 traffic as SSL*<br>
```bash
python protoShark/dissect/server.py /tmp/clientServerPipe -d tcp.port==8883,ssl -r examples/sample.pcapng
```

*Run dissect Server. Treat all TCP 8883 traffic as MQTT under SSL. Use server.key file to decrypt SSL. Write SSL debug logs to ssldebug_proto.log*
```bash
python protoShark/dissect/server.py /tmp/clientServerPipe -o ssl.debug_file:ssldebug_proto.log -o ssl.keys_list:127.0.0.1,8883,mqtt,server.key -r mqtttls.pcap
```

*Create dissect Client that connects to Server and reads each packet in sequence. Packets are native Python objects that can be modified, serialized, unserialized, and written to pcap files or interfaces.*<br>
```python
from protoShark.dissect import Client
from protoShark.packets import Packet
from protoShark.write import FileWriter
import binascii,sys

#connecting to server started with following command
#starting from top-level protoShark directory
#python protoShark/dissect/server.py /tmp/clientServerPipe -d tcp.port==8883,ssl -r examples/sample.pcapng

inPipe = r'/tmp/clientServerPipe'
c=Client(inPipe)
pkts=[]

#Collect all Packets from dissect server and save them in pkts list.
try:
    c.connect()
except Exception as e:
    print (e)
    sys.exit(0)

try:
    while(True):
        pkt=c.read_next()
        if(pkt is None):
            break
        pkts.append(pkt)

except Exception as e:
    print e

#get Client Hello sample packet, the fourth packet in the original pcap file.
print("got ssl client hello packet")
clientHelloPkt=pkts[3]

#get client hello Packet attributes.
pktAttrs=clientHelloPkt.get_attributes()

#get ssl data from client hello Packet
#packet attributes are indexed by their 'abbrev' feature. This value is NOT unique. Therefore,
#the below returns a list. You must index into the list to get the attribute(s).
print ("got packet ssl attribute")
sslDataList=pktAttrs['ssl']
sslData=sslDataList[0]

#get tcp data from client hello Packet
print ("got packet tcp attribute")
tcpDataList=pktAttrs['tcp']
tcpData=tcpDataList[0]

#print ssl attribute
print ("examine ssl attribute top-level.")
print(sslData)
print ("")

#print tcp attribute
print ("examine tcp attribute top-level.")
print(tcpData)
print ("")

#examine ssl data
print("walk packet tree using ssl attribute as tree root.")
clientHelloPkt.walk_print(sslData)

#get list of ssl extensions and print each one
print("examine ssl extensions in client hello")
extensions=pktAttrs['ssl.handshake.ciphersuite']

#each entry is its own attribute with its own set of features that can be
#examined: abbrev, name, blurb, representation, fvalue, data, level, id, parentId
#offset, etype, and ftype.
for each in extensions:
    print each.get_representation()

#serialize Packet
spkt=clientHelloPkt.serialize_to_string()
print ("serialized packet to string of length %d")%len(spkt)

#unserialize Packet
upkt=Packet.parse_from_string(spkt)
print ("unserialized packet.")

#print ssl attribute(nonrecursively) to verify success.
ussl=upkt.get_attributes()['ssl'][0]
print(ussl)

if str(ussl) != str(sslData):
    print ("Error: parsed ssl not same as pre-serialized ssl.")

else:
    print("Serialize-Parse succeeded.")


#modify each packet by changing port from 8883 to 1111
#we don't fix tcp checksum in this demo. Should do this in real application.
#write modified packets to new pcap file
fw=FileWriter()
errbuf=fw.make_pcap_error_buffer()
outfile=fw.open(r'modifiedSample.pcap', errbuf)

for each in pkts:
    src=each.get_attributes()['tcp.srcport'][0]
    dst=each.get_attributes()['tcp.dstport'][0]

    #this is 1111 represented as hex string
    newHexString='0457'

    #Important note about Packet.set_attribute(): this function ONLY modifies the raw packet data.
    #It does NOT change the attributes' non-data features such as 'representation' or 'fvalue'.
    #Note that it's the raw packet data that gets written out to pcap file, so this is ok.
    if src.get_fvalue() == '8883':
        each.set_attribute(newHexString,'tcp.srcport')

    elif dst.get_fvalue() == '8883':
        each.set_attribute(newHexString,'tcp.dstport')

    utime, ltime = each.get_time()
    pktLen=each.get_num_bytes()

    newPktData = binascii.a2b_hex(each.get_pkt_data())
    fw.write(outfile,utime,ltime,pktLen,newPktData,errbuf)

fw.close(outfile)

```

*Run Client*<br>
```bash
python protoSharkClient.py
```

See protoShark/examples/out.txt for resultant output.<br>
