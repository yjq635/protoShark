#!/usr/bin/python

###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################

import protoShark.packets.protoShark_pb2 as protoShark_pb2
import struct, types, sys
from protoShark.packets import Packet 


def read_next_message(inPipeStream):
    msgSize = 0
    pktTree = None
    
    try:
        pktTree=protoShark_pb2.PacketAttributeTree()
        sizeData=inPipeStream.read(4)
        if(len(sizeData)>0):
            msgSize=struct.unpack('@I',sizeData)
            pktTree.ParseFromString(inPipeStream.read(msgSize[0]&0xffffffff))
        else:
            pktTree=None
            
        
    except Exception as e:
        raise e
    
    return pktTree

class Client(object):
    def __init__(self, pipeName):
        self.pipeName = pipeName
        self.fd=None

    def connect(self):
        fd=None
        try:
            fd = open(self.pipeName,'rb')
            self.fd=fd

        except Exception as e:
            raise(e)
        

    def read_next(self):
        
        if(self.fd is None):
            raise AttributeError("AttributeError: Client not connected.")
        
        try:
            msg=read_next_message(self.fd)
        except Exception as e:
            raise(e)
        
        try:
            if msg is not None:
                pkt=Packet(msg)
            else:
                return None
        except Exception as e:
            raise(e)
        
        return pkt
    
    def disconnect(self):
        if(self.fd is not None):
            self.fd.close()
            

#test main
if __name__ == "__main__":

    inPipe = r'/tmp/bobpipe'
    c=Client(inPipe)

    try:
        c.connect()
    except Exception as e:
        print (e)
        sys.exit(0)
    
    try:
        while(True):
            pkt=c.read_next()
            if(pkt is None):
                break            

            pkt.walk_print()
            
    except Exception as e:
        print e

