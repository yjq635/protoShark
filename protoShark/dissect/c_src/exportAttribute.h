/* exportAttribute.h
 *
 * Exports packet dissection data as Google Protocol Buffer object.
 *
 * Author: Mark Landriscina<mlandri@verizon.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef EXPORTATTRIBUTE_H_
#define EXPORTATTRIBUTE_H_

#include <stdint.h>

//Same definition as in sharkPy.h. MUST be same.
typedef struct {

        char *abbrev;
        char *name;
        char *blurb;
        char *representation;
        char *fvalue;
        char *data;
        uint32_t level;
        uint32_t id;
        uint32_t parent_id;
        int32_t offset;
        uint8_t etype;
        uint8_t ftype;
        int32_t start;

}attribute;

extern "C" void exportAttribute(attribute *);
extern "C" void closeNamedPipe(const char *name, int32_t namedPipeFd);
extern "C" int32_t createNamedPipe(const char *name);

#endif /* EXPORTATTRIBUTE_H_ */

/*
 * Editor modelines  -  https://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 2
 * tab-width: 8
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=2 tabstop=8 expandtab:
 * :indentSize=2:tabSize=8:noTabs=true:
 */
