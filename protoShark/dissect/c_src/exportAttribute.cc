
/* exportAttribute.cc
 *
 * Exports packet dissection data as Google Protocol Buffer object.
 *
 * Author: Mark Landriscina<mlandri@verizon.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "exportAttribute.h"
#include "protoShark.pb.h"
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>


//for named pipe, mkfifo
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

using namespace std;
using namespace google::protobuf::io;

#ifndef NDEBUG
//Write packet attribute tree structure to stdout
//Useful for debugging. Not exported.
void treePrintWalk(Attribute& treeRoot, PacketAttributeTree& pktTree)
{

	uint32_t i=0;

    for(i=0 ; i<treeRoot.level(); ++i)
    {
    	cout << "  ";
    }

	::google::protobuf::RepeatedField<uint32_t>::iterator itr = treeRoot.mutable_childattributesids()->begin();
	cout << treeRoot.abbrev() << ", " << treeRoot.level() << endl;
	for(; itr != treeRoot.mutable_childattributesids()->end(); itr++)
	{
		Attribute& child = (*pktTree.mutable_attributedictionary())[*itr];
		treePrintWalk(child, pktTree);
	}

}
#endif

int32_t pipeFileDescriptor = -1;
const char *pipeName = NULL;

int32_t
createNamedPipe(const char *name)
{
	int32_t namedPipeFd = -1;

	if(NULL == name)
	{
		goto done;
	}

	mkfifo(name, S_IRUSR|S_IWUSR);
	namedPipeFd=open(name, O_WRONLY);
	pipeFileDescriptor = namedPipeFd;
	pipeName = name;

done:
	return namedPipeFd;
}

void
closeNamedPipe(const char *name, int32_t namedPipeFd)
{
	if(0 < namedPipeFd && pipeFileDescriptor == namedPipeFd && name == pipeName)
	{
		close(namedPipeFd);
		namedPipeFd=-1;
		pipeFileDescriptor = namedPipeFd;
		unlink(name);
	}
}

void
serializeAndWrite(PacketAttributeTree& out, int32_t namedPipeFd)
{

	if(0 < namedPipeFd)
	{
		string data = out.SerializeAsString();
		uint32_t dataSize = data.size();

		ZeroCopyOutputStream *raw_output = new FileOutputStream(namedPipeFd);
		CodedOutputStream* coded_output = new CodedOutputStream(raw_output);
		coded_output->WriteRaw(&dataSize, 4);
		coded_output->WriteRaw(data.c_str(), data.size());

		delete coded_output;
		delete raw_output;
	}
}


//This function is passed into sharkPy. It converts attributes to Attribute objects.
//After all packet attributes have been collected, it recreates packet dissection tree
//hierarchy within a packetAttributeTree object. The packetAttribute object is then
//serialized and written out subscriber.
extern "C" void
exportAttribute(attribute *attrib) {

	static PacketAttributeTree pktTree;

	//Do nothing attrib is NULL or if named pipe isn't available.
	if(NULL == attrib) {
	    goto done;
	}

	//If true, then this attribute is a sentinel signaling that packetTree is complete.
	//We can build a tree.
	if(Attribute_exportType_EXPORTED_FINI == attrib->etype)
	{
		//Drop any packets that don't provide a root node
		if(0 != pktTree.rootid())
		{
			//Attribute& root = (*pktTree.mutable_attributedictionary())[pktTree.rootid()];
			//treePrintWalk(root, pktTree);
			serializeAndWrite(pktTree, pipeFileDescriptor);
		}
		pktTree.Clear();
	}

	else
	{
		//Create temp Attribute on stack.
		Attribute nxt;
		(NULL != attrib->abbrev) ? nxt.set_abbrev(attrib->abbrev) : nxt.set_abbrev("");
		(NULL != attrib->name) ? nxt.set_name(attrib->name) : nxt.set_name("");
		(NULL != attrib->blurb) ? nxt.set_blurb(attrib->blurb) : nxt.set_blurb("");
		(NULL != attrib->representation) ? nxt.set_representation(attrib->representation) : nxt.set_representation("");
		(NULL != attrib->fvalue) ? nxt.set_fvalue(attrib->fvalue) : nxt.set_fvalue("");
		(NULL != attrib->data) ? nxt.set_data(attrib->data) : nxt.set_data("");
		nxt.set_level(attrib->level);
		nxt.set_id(attrib->id);
		nxt.set_parentid(attrib->parent_id);
		nxt.set_offset(attrib->offset);
		nxt.set_etype((Attribute_exportType)attrib->etype);
		nxt.set_ftype((Attribute_fieldType)attrib->ftype);
		nxt.set_start(attrib->start);

		//insert copy of nxt into pktTree attribute dictionary
		//and add id to parent's child id list.
		if(0 == pktTree.attributedictionary().count(nxt.id()))
		{
			//Drop any Attributes that don't have a parent.
			if(0 < pktTree.attributedictionary().count(nxt.parentid()))
			{
				Attribute& parent = (*pktTree.mutable_attributedictionary())[nxt.parentid()];
				parent.mutable_childattributesids()->Add(nxt.id());
			}
			else if(nxt.id() != nxt.parentid())
			{
				goto done;
			}

			//The below call makes a deep copy of nxt and inserts the copy into
			//the attribute dictionary.
			pktTree.mutable_attributedictionary()->insert({nxt.id(), nxt});

			//Record id of packet root attribute.
			if(nxt.id() == nxt.parentid())
			{
				pktTree.set_rootid(nxt.id());
			}
		}
	}

done:
	;

}

/*
 * Editor modelines  -  https://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 2
 * tab-width: 8
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=2 tabstop=8 expandtab:
 * :indentSize=2:tabSize=8:noTabs=true:
 */
