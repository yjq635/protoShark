#!/usr/bin/python

###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################

import os,ctypes,ctypes.util,re,sys,site,time,multiprocessing, re


#################################################################################################
# class attribute:
#
#Interfaces with c-code. Returned from callback function. As such it is a utility class
#and users should never see objects created from this class definition.
class attribute(ctypes.Structure):
    _fields_ = [  ("abbrev", ctypes.c_char_p),
                  ("name", ctypes.c_char_p),
                  ("blurb", ctypes.c_char_p),
                  ("representation", ctypes.c_char_p),
                  ("fvalue", ctypes.c_char_p),
                  ("data", ctypes.c_char_p),
                  ("level",ctypes.c_uint32),
                  ("id",ctypes.c_uint32),
                  ("parent_id", ctypes.c_uint32),
                  ("offset", ctypes.c_int),
                  ("type", ctypes.c_uint8),
                  ("ftype", ctypes.c_uint8),
                  ("start", ctypes.c_uint)]

dissect=None
setExport=None
exportDissectionAttribute = None
createNamedPipe = None
closeNamedPipe = None

def getDumpcapExecPath():

    dirs = os.environ['PATH']
    dirlist=dirs.split(os.pathsep)
    sepr=os.path.sep
    rtnpath=None

    for path in dirlist:
        dlist = os.listdir(path)
        if(r'dumpcap' in dlist):
            rtnpath=path
            break
        elif(r'dumpcap.exe' in dlist):
            rtnpath=path
            break

    if(None == rtnpath):
        raise RuntimeError("Could not locate dumpcap executable in PATH. wire_dissect will not run")

    return rtnpath


def set_libs():

    SAVE_ATTR=ctypes.CFUNCTYPE(ctypes.c_bool, ctypes.POINTER(attribute))

    #libprotoShark=r'/home/me/protoShark/build/lib.linux-x86_64-2.7/protoShark_dissect.so'
    libprotoShark = None

    slpath=[site.getsitepackages()[0]]

    for each_dir in slpath:
        if(not os.path.isdir(each_dir)):
            raise RuntimeError("Path %s not found. Check path and try again."%each_dir)

        names = [name for name in os.listdir(each_dir)
                if os.path.isfile(os.path.join(each_dir,name))]

        for name in names:
            if (re.search(r'protoShark_dissect\.so', name) is not None):
                if(libprotoShark is None):
                    libprotoShark=os.path.join(each_dir,name)

    try:
        protoSharklib=ctypes.CDLL(libprotoShark, mode=ctypes.RTLD_GLOBAL)
    except Exception as e:
        raise(e)

    global dissect
    dissect=protoSharklib.run
    dissect.arg_types=[ctypes.c_int, ctypes.POINTER(ctypes.c_char_p)]
    dissect.restype=ctypes.c_int

    global closeNamedPipe
    closeNamedPipe=protoSharklib.closeNamedPipe
    closeNamedPipe.arg_types=[ctypes.c_int32, ctypes.POINTER(ctypes.c_char_p)]
    closeNamedPipe.restype=None

    global createNamedPipe
    createNamedPipe=protoSharklib.createNamedPipe
    createNamedPipe.arg_types=[ctypes.POINTER(ctypes.c_char_p)]
    createNamedPipe.restype = ctypes.c_int

#
# IMPORTANT: multiple servers CANNOT be run in same process!!
# When in doubt, use Server.create_as_process()
#
class Server(object):
    
    valid=True
    
    def __init__(self, pipeName, commandLine):
        self.pipeName = pipeName
        self.commandLine = commandLine
        self.options=re.sub("\s{2,}"," ",re.sub("[\t\n]"," ",commandLine.strip())).split(" ")
        self.writePipe=None

        #Acquire required functionality
        try:
            set_libs()
        except Exception as e:
            raise(e)

    def start(self):

        if True == Server.valid:
            Server.valid = False
        else:
            raise RuntimeError("Multiple server instances cannot be run in same process. A server instance has already run in this process.")

        if(0 == len(self.options)):
            raise AttributeError("Attribute Error: Failed to provide required option information.")

        dPath= getDumpcapExecPath()

        if(0 == len(dPath)):
            raise AttributeError("Attribute Error: Did not find dumpcap executable. Verify that it is in cmd path.")

        cmd_options=[dPath]

        for each in self.options:
            cmd_options.append(each)

        #number of command line args
        argc = ctypes.c_int(len(cmd_options))

        #compute total number of required option string bytes and create
        #C buffer that will be passed to function call.
        sz=len(dPath)
        for each in cmd_options:
            sz+=len(str(each))

        myargv = ctypes.c_char_p *(sz)
        argv = myargv()

        #construct options string
        for idx in xrange(len(cmd_options)):
            argv[idx]=cmd_options[idx]

        Argv = ctypes.pointer(argv)

        try:
            self.writePipe=createNamedPipe(self.pipeName)

            global dissect
            dissect(argc,Argv)
        except Exception as e:
            raise (e)

        finally:
            closeNamedPipe(self.pipeName,self.writePipe)
            if os.path.exists(self.pipeName):
                os.unlink(self.pipeName)
    
    @staticmethod
    def _start_server(pipeName, commandLine):
        try:

            s = Server(pipeName, commandLine)
            if s is not None:
                s.start()
        except Exception as (e):
            print(e)
        
    @staticmethod
    def create_as_process(pipeName, commandLine):
        p=multiprocessing.Process(target=Server._start_server, args=(pipeName, commandLine))
        p.daemon=True
        p.start()
        return p

if __name__ == "__main__":

    if(3 > len(sys.argv)):
        print("usage: %s <outpipename> [options] [infilename]" % sys.argv[0])
        sys.exit(0)

    s=None
    options=' '.join(sys.argv[2:])
    pname=str(sys.argv[1])

    try:
        s = Server(pname, options)
        if s is not None:
            s.start()
    except Exception as (e):
        print(e)





