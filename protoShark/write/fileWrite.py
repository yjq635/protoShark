#!/usr/bin/python


###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################


import ctypes,ctypes.util,os,re,site,sys
from Queue import Queue
from threading import Thread, Event
import binascii



#################################################################################################
# class FileWriter:
#
# Class to open/create pcap dump file and write packets into it.
#################################################################################################
class FileWriter(object):

    
    def __init__(self,):
        self.interface_list=[]
        self.send_interfaces_list=[]

        self.pcaplib = None;
        self.writerlib = None
        
        self.pcap_lib_path = None
        self.writer_lib_path = None

        self.get_error_buffer_size = None
        self.pcap_close = None
        self.pcap_write_file = None
        self.pcap_write_packet = None

        self.slpath = site.getsitepackages()
        self.set_api()

    def set_api(self):
        
        libwriter=None

        for each_dir in self.slpath:
            if(not os.path.isdir(each_dir)):
                raise RuntimeError("RuntimeError: Path %s not found. Check path and try again."%each_dir)
            
            if libwriter is not None:
                break

            names = [name for name in os.listdir(each_dir)
                    if os.path.isfile(os.path.join(each_dir,name))]
            
            for name in names:
                            
                if (re.search(r'protoShark_write\.so', name) is not None):
                    libwriter=os.path.join(each_dir,name)
                    break

        if libwriter is None:
            raise RuntimeError("RuntimeError: Could not locate protoShark_write.")

        self.writer_lib_path=libwriter        

        self.writerlib=ctypes.CDLL(self.writer_lib_path)
        
        self.get_error_buffer_size=self.writerlib.getErrorBufferSize
        self.get_error_buffer_size.argtypes=[]
        self.get_error_buffer_size.restype=ctypes.c_uint
        
        self.open = self.writerlib.open_pcap_file
        self.open.argtypes=[ctypes.c_char_p, ctypes.c_char_p]
        self.open.restype=ctypes.c_void_p
        
        self.write = self.writerlib.pcap_write_packet
        self.write.arg_types=[ctypes.c_void_p,
                                          ctypes.c_int64, 
                                          ctypes.c_int32, 
                                          ctypes.c_uint32, 
                                          ctypes.c_char_p, 
                                          ctypes.c_char_p]
        self.write.restype=ctypes.c_int32
               
        self.close = self.writerlib.close_pcap_dump
        self.close.arg_types=[ctypes.c_void_p]
        self.close.restype = None

    def make_pcap_error_buffer(self,):
        err_buf_size = self.get_error_buffer_size()
        err_buf = ctypes.create_string_buffer(err_buf_size)
        return err_buf
    

#TEST main#####TEST main#####TEST maim########TEST main#####TEST main#####TEST main#####TEST maim########TEST main
if __name__=='__main__':


    from protoShark.dissect import Client

    #client acquires packets from dissect.Server via named pipe
    inPipe = r'/tmp/clientServerPipe'
    c=Client(inPipe)
    
    #open new pcap file that we will write modified packets to
    fw=FileWriter()
    errbuf=fw.make_pcap_error_buffer()
    outfile=fw.open(r'/home/me/test_output_file.pcap', errbuf)

    #connect to dissect.Server named pipe
    try:
        c.connect()
    except Exception as e:
        print (e)
        sys.exit(0)
    
    #read packets from dissect.Server one at a time
    #modify packet and write modified packet to new pcap file
    try:
        for _ in xrange(10):
            pkt=c.read_next()
            if(pkt is None):
                break            

            pkt.set_attribute('000000000000', 'eth.src', mustExist=True)
            pkt.set_attribute('111111111111', 'eth.dst', mustExist=True)
            
            utime, ltime = pkt.get_time()
            dataLen=len(pkt.get_root().get_data())/2
            
            newPktData = binascii.a2b_hex(pkt.get_root().get_data())
            fw.write(outfile,utime,ltime,dataLen,newPktData,errbuf)            

            #pkt.walk_print()
            
    except Exception as e:
        print e
    
    fw.close(outfile)

