#!/usr/bin/python


###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################

import ctypes,ctypes.util,os,re,site,sys
from Queue import Queue
from threading import Thread, Event
import binascii

#################################################################################################
# class _interface_info:
#
#Interfaces with c-code. Returned from callback function. As such it is a utility class
#and users should never see objects created from this class definition.
#################################################################################################
class _interface_info(ctypes.Structure):
    _fields_ = [("flags", ctypes.c_uint),
                ("description", ctypes.c_char_p),
                ("name", ctypes.c_char_p),
                ("openable", ctypes.c_uint),
                ("datalink_types", ctypes.c_char_p)]

#################################################################################################
# class interface_info:
#
#_Creates an interface info object which is part of user API. Describes information that
# PCAP provides about a given interface.
#################################################################################################
class interface_info(object):
    def __init__(self, iface_structure):

        self.description = iface_structure.contents.description
        self.name = iface_structure.contents.name
        self.flags = iface_structure.contents.flags
        self.openable = False
        self.datalink_types = None 
        
        if(0 < iface_structure.contents.openable):
            self.openable = True
            self.datalink_types = iface_structure.contents.datalink_types
            
#################################################################################################
# class FileWriter:
#
# Class to open/create pcap dump file and write packets into it.
#################################################################################################
class WireWriter(object):

    def __init__(self,):
        self.interface_list=[]
        self.send_interfaces_list=[]

        self.pcaplib = None;
        self.writerlib = None
        
        self.pcap_lib_path = None
        self.writer_lib_path = None

        self.set_interface_callback = None
        self.get_number_of_interfaces = None
        self.get_error_buffer_size = None
        self.get_interface_list = None
        self.pcap_open_live = None
        self.pcap_inject = None
        self.pcap_close = None
        
        #callback functionality to export inteface information 
        self.SAVE_IFACE = ctypes.CFUNCTYPE(ctypes.c_bool, ctypes.POINTER(_interface_info))
        self.copy_out_iface=self.SAVE_IFACE(self.save_iface)

        self.slpath = site.getsitepackages()
        self.set_api()

    def set_api(self):
        
        libwriter=None
        libpcap=None

        for each_dir in self.slpath:
            if(not os.path.isdir(each_dir)):
                raise RuntimeError("RuntimeError: Path %s not found. Check path and try again."%each_dir)

            if libwriter is not None:
                break
            
            names = [name for name in os.listdir(each_dir)
                    if os.path.isfile(os.path.join(each_dir,name))]
                        
            for name in names:
                            
                if (re.search(r'protoShark_write\.so', name) is not None):
                    libwriter=os.path.join(each_dir,name)
                    break
                
        if(libpcap is None):
            libpcap = ctypes.util.find_library("pcap")
            
        if libwriter is None:
            raise RuntimeError("RuntimeError: Could not locate protoShark_write.")
        
        if(libpcap is None):
            raise RuntimeError("Could not locate libpcap.")

        self.pcap_lib_path=libpcap
        self.pcaplib = ctypes.CDLL(self.pcap_lib_path, mode=ctypes.RTLD_GLOBAL)
      
        self.writer_lib_path=libwriter        
        self.writerlib=ctypes.CDLL(self.writer_lib_path)
        
        self.get_error_buffer_size=self.writerlib.getErrorBufferSize
        self.get_error_buffer_size.argtypes=[]
        self.get_error_buffer_size.restype=ctypes.c_uint
        
        self.get_number_of_interfaces=self.writerlib.getNumberOfInterfaces
        self.get_number_of_interfaces.argtypes=[ctypes.c_char_p]
        self.get_number_of_interfaces.restype=ctypes.c_int
        
        self.get_interface_info_list=self.writerlib.getInterfaceInfoList
        self.get_interface_info_list.argtypes=[ctypes.c_char_p]
        self.get_interface_info_list.restype=ctypes.c_int
        
        self.set_interface_callback = self.writerlib.setInterfaceExportCallback
        self.set_interface_callback.argtypes=[self.SAVE_IFACE]
        self.set_interface_callback.restype=None
        
        self.pcap_open_live = self.pcaplib.pcap_open_live
        self.pcap_open_live.argtypes = [ctypes.c_char_p, ctypes.c_uint, ctypes.c_uint, ctypes.c_uint, ctypes.c_char_p]
        self.pcap_open_live.restype = ctypes.c_void_p
        
        self.pcap_inject = self.pcaplib.pcap_inject
        self.pcap_inject.argtypes=[ctypes.c_void_p, ctypes.c_void_p, ctypes.c_int]
        self.pcap_inject.restype=ctypes.c_int
        
        self.pcap_close = self.pcaplib.pcap_close
        self.pcap_close.argtypes = [ctypes.c_void_p]
        self.pcap_close.restype = None
        
        #set Python callback for C-code
        self.set_interface_callback(self.copy_out_iface)        


    def make_pcap_error_buffer(self,):
        err_buf_size = self.get_error_buffer_size()
        err_buf = ctypes.create_string_buffer(err_buf_size)
        return err_buf
    
    def save_iface(self, in_iface):
        if(in_iface is None):
            raise ValueError("_interface not provided in functional call.")
        new_iface=interface_info(in_iface)
        self.interface_list.append(new_iface)
        
    def verify_send_interfaces(self, list_of_iface_names):
        err_buf = self.make_pcap_error_buffer()
        name_list=[]

        #re-kerjigger lists
        for each in self.send_interfaces_list:
            self.send_interfaces_list.remove(each)

        for each in self.interface_list:
            self.interface_list.remove(each)

        if( 0 > self.get_interface_info_list(err_buf) ):
            raise RuntimeError("Pcap error: "+str(err_buf.value))
        
        for each in self.interface_list:
            name_list.append(each.name)
        
        for each in list_of_iface_names:
            
            if (each == 'any'):
                raise ValueError("Cannot write to Psuedo-device 'any'.")

            if (each not in name_list):
                raise ValueError("Interface with name "+str(each)+" not available.")
            
    def open_interfaces_for_sending(self, list_of_iface_names):
        #Make error buffer
        err_buf = self.make_pcap_error_buffer()

        try:
            #Check if we can (likely) write to interface
            self.verify_send_interfaces(list_of_iface_names)
        except Exception as e:
            raise e

        #Call pcap_open_live to get interface descriptors, which
        #will be squirreled away in self.send_interfaces_list
        for each in list_of_iface_names:

            rst = self.pcap_open_live(each, 65535, 0, 0, err_buf)
            if rst is None:
                raise RuntimeError("Failed to open interface "+str(each)+". Pcap error: "+str(err_buf.value))

            #Append tuple (iface name, iface descriptor
            self.send_interfaces_list.append( (each, rst) )

    def close_sending_interfaces(self, iface_list=[]):
        #Default is to close all interfaces. However individual interfaces can be
        #closed by specifying them by name and passing those names into this function
        #in a list.
        if iface_list is None or len(iface_list) == 0:
            for each in self.send_interfaces_list:
                self.pcap_close(each[1])

        else:
            names = []
            for each in self.send_interfaces_list:
                names.append(each[0])

            for name in iface_list:
                if( name not in names ):
                    raise ValueError("Provided interface name is not open or is unknown, "+str(name))

                self.pcap_close(name)

    #returns number of bytes written
    def write(self, bufr, stripLastByte=False):

        if(0 == len(self.send_interfaces_list)):
            raise AttributeError("AttributeError: no interfaces opened for write.")
        
        if(0 == len(bufr)):
            return 0                
        
        for iface in self.send_interfaces_list:
            name=iface[0]
            ifc=iface[1]
            
            #create_string_buffer adds a trailing NULL
            write_bfr=ctypes.create_string_buffer(bufr)
            bLen=len(write_bfr)
            
            #we do NOT want trailing NULL, so subtract 1 from len(write_bfr)
            if stripLastByte:
                bLen -= 1
            
            try:
                rtn=self.pcap_inject(ifc, write_bfr, bLen)
            except Exception as e:
                raise e
        return rtn                        
    

#TEST main#####TEST main#####TEST maim########TEST main#####TEST main#####TEST main#####TEST maim########TEST main
if __name__=='__main__':


    if 3 > len(sys.argv):
        print("usage: %s data interfaceName [interfaceName(s)]" % sys.argv[0])
        sys.exit(0)
        
    #These interfaces will be written to
    interfaces=[]
    for each in sys.argv[2:]:
        interfaces.append(each)
    
    data=sys.argv[1]
        
    ww=WireWriter()   
    ww.open_interfaces_for_sending(interfaces)
    for each in ww.send_interfaces_list:
        try:
            numBytesWritten=ww.write(data, stripLastByte=True)
            print "wrote %d bytes"%numBytesWritten
        except Exception as e:
            print e
    
    #by default will close all opened interfaces
    ww.close_sending_interfaces()

