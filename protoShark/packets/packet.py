#!/usr/bin/python

###########################################################################
#
# author: Mark Landriscina <mlandri@verizon.net>
# Created on: Jul 10, 2017
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
############################################################################


import protoShark_pb2
import struct, types
import binascii

_ftype_desc = ["FT_NONE",
      "FT_PROTOCOL",
      "FT_BOOLEAN",
      "FT_UINT8",
      "FT_UINT16",
      "FT_UINT24",
      "FT_UINT32",
      "FT_UINT40",
      "FT_UINT48",
      "FT_UINT56",
      "FT_UINT64",
      "FT_INT8",
      "FT_INT16",
      "FT_INT24",
      "FT_INT32",
      "FT_INT40",
      "FT_INT48",
      "FT_INT56",
      "FT_INT64",
      "FT_FLOAT",
      "FT_DOUBLE",
      "FT_ABSOLUTE_TIME",
      "FT_RELATIVE_TIME",
      "FT_STRING",
      "FT_STRINGZ", 
      "FT_UINT_STRING", 
      "FT_ETHER",
      "FT_BYTES",
      "FT_UINT_BYTES",
      "FT_IPv4",
      "FT_IPv6",
      "FT_IPXNET",
      "FT_FRAMENUM", 
      "FT_PCRE", 
      "FT_GUID", 
      "FT_OID", 
      "FT_EUI64",
      "FT_AX25",
      "FT_VINES",
      "FT_REL_OID",
      "FT_SYSTEM_ID",
      "FT_STRINGZPAD", 
      "FT_FCWWN",
      "FT_NUM_TYPES"]

def read_next_message(inPipeStream):
    msgSize = 0
    pktTree = None
    
    try:
        pktTree=protoShark_pb2.PacketAttributeTree()
        sizeData=inPipeStream.read(4)
        if(len(sizeData)>0):
            msgSize=struct.unpack('@I',sizeData)
            pktTree.ParseFromString(inPipeStream.read(msgSize[0]&0xffffffff))
        else:
            pktTree=None
            
        
    except Exception as e:
        raise e
    
    return pktTree
    
def _tree_walk(tree, node, funct, aux=None):
    funct(node,aux)
    for id in node.childAttributesIds:
        nxt=tree.attributeDictionary[id]
        _tree_walk(tree, nxt, funct, aux)

#do not check string length if varLength != 0
def validate_hex(var, varName):

    varLength = len(var)

    if 0 >= varLength or 0 != varLength%2:
        raise AttributeError("%s must be have even number of hex characters greater than one."%varName)
        
    try:
        v=int(var,16)
    except Exception as e:
        raise (e)
   
#wraps protoShark_pb2.PacketAttributeTree
class Packet(object):
        
    #wraps protoShark_pb2.Attribute
    class Attribute(object):
        def __init__(self, inAttribute, parentPkt):
            self._parent=parentPkt
            self._attribute=inAttribute
            self._modifiedData=""
            self._indent=''
            
        #Validate that attribute.data == corresponding slice from frame.data
        def _assert_data_consistent(self):
            if self._attribute.data != self.get_data():
                raise AttributeError("%s. Inconsistent attribute data.\nLocal:        %s.\nPacket-level: %s.\nThis error can arise if you've already modified this attribute. Alternatively, this could be a field that is computed/derived and not represented directly in Packet data." % (self._attribute.abbrev, self._attribute.data, self.get_data()))
        
        def __str__(self):
            rtn=self._indent+'Attribute:\n'
            rtn+=self._indent+' %-15s\t\t%s\n'%('abbrev',self.get_abbrev())
            rtn+=self._indent+' %-15s\t\t%s\n'%('name',self.get_name())
            rtn+=self._indent+' %-15s\t\t%s\n'%('blurb',self.get_blurb())
            rtn+=self._indent+' %-15s\t\t%s\n'%('fvalue',self.get_fvalue())
            rtn+=self._indent+' %-15s\t\t%s\n'%('level',str(self.get_level()))
            rtn+=self._indent+' %-15s\t\t%s\n'%('offset',str(self.get_offset()))
            rtn+=self._indent+' %-15s\t\t%s\n'%('ftype',str(self.get_ftype()))
            rtn+=self._indent+' %-15s\t\t%s\n'%('ftype_desc',_ftype_desc[self.get_ftype()])
            rtn+=self._indent+' %-15s\t\t%s\n'%('representation',self.get_representation())
            rtn+=self._indent+' %-15s\t\t%s\n'%('data',self.get_data())
            rtn+='\n'
            
            attribute_children=self.get_children()
            rtn+=self._indent+'Number of children: %d\n'%len(attribute_children)
            for each in attribute_children:
                rtn+=self._indent+' %s'%each.get_abbrev()
                if(each != attribute_children[-1]):
                    rtn+='\n'

            return rtn
        
        def get_children(self):
            rtn=[]
            for each in self._attribute.childAttributesIds:
                child=self._parent._packetAttributesById.get(each)
                if child is not None:
                    rtn.append(child)
            return rtn
        
        def is_modified(self):
            if(0 == len(self._modifiedData)):
                return False
            return True
        
        #getters
        def get_abbrev(self):
            return self._attribute.abbrev
        
        def get_name(self):
            return self._attribute.name

        def get_blurb(self):
            return self._attribute.blurb
        
        def get_representation(self):
            return self._attribute.representation
        
        def get_fvalue(self):
            return self._attribute.fvalue
        
        def get_data(self):
            frame=self._parent._packetAttributes['frame'][0]
            frameData = frame._attribute.data
            rtnData = frameData[self.get_hex_offset():self.get_hex_offset() + len(self._attribute.data)]

            return rtnData
        
        def get_level(self):
            return self._attribute.level
        
        def get_id(self):
            return self._attribute.id
        
        def get_parentId(self):
            return self._attribute.parentId
        
        def get_offset(self):
            return self._attribute.offset
        
        def get_hex_offset(self):
            return self._attribute.offset * 2
        
        def get_etype(self):
            return self._attribute.etype
        
        def get_ftype(self):
            return self._attribute.ftype
        
        #setters
        
        def set_data(self, value, enforceConsistant=True):
            frame=self._parent._packetAttributes['frame'][0]
            frameData = frame._attribute.data
            currentData = frameData[self.get_hex_offset():self.get_hex_offset() + len(self._attribute.data)]
            
            #data stored in hex, so all replacement values must have an even number of characters
            if(0 != len(value)%2):
                raise AttributeError(" Replacement value does not have even number of hex characters.")
                        
            if enforceConsistant:
                if(len(currentData)!= len(value)):
                    raise AttributeError("New attribute data value not same length as original data. New length %u. Original length %u." %(len(value), len(currentData)))
                
                #Wireshark doesn't handle field offset and length values consistently. This is particularly problematic for derived fields
                #We should only permit set_data to set fields that have offsets and lengths that are consistent in both Frame.data and field.data.
                self._assert_data_consistent()
            
            #Modified data value does not take effect until Packet.commit_changes() is called. 
            #Stage new requested value here. 
            self._modifiedData = value
            
            #Add self reference to parent modifiedAttributes list
            self._parent._modifiedAttributes.append(self)    
    
    def __init__(self, packetAttributeTree):
        

        self._pktTree = packetAttributeTree
        self._modifiedPacketData=""        
        self._isInitialized = False
        self._modifiedAttributes=[]
        self._packetAttributes={}
        self._packetAttributesById={}
        self._set_packet_attributes()
        self._root = self._packetAttributes['frame'][0]

        #Packet object has been initialized.
        self._isInitialized = True
        
    def get_root(self):
        return self._root
    
    def get_attributes(self):
        return self._packetAttributes
        
    def _commit_change(self):
                
        for each in self._modifiedAttributes:
            frame = self._packetAttributes['frame'][0]
            frameData = frame._attribute.data
            
            for each in self._modifiedAttributes:
                newData=frameData[:each.get_hex_offset()]
                newData+=each._modifiedData
                newData+=frameData[len(newData):]
                frame._attribute.data=newData
                
        del self._modifiedAttributes[:]
                

    def _set_packet_attributes(self):
        
        def _assign(node,attribDict):
            if node.abbrev not in attribDict[0].keys():
                attribDict[0][node.abbrev]=[]
            
            pa = Packet.Attribute(node, self)
            attribDict[0][node.abbrev].append(pa)
            attribDict[1][node.id]=pa

        rootNode = self._pktTree.attributeDictionary.get(self._pktTree.rootId)

        if(rootNode is not None):
            _tree_walk(self._pktTree, rootNode, _assign, (self._packetAttributes, self._packetAttributesById))
        
    def walk_print(self,node=None):
        
        n=node
        if(n is None):
            n=self._root
        
        level=n.get_level()
        n._indent='    '*level
        s=str(n)
        s+='\n\n'
        print(s)
        n._indent=''
            
        attributeChildren=n.get_children()
        for child in attributeChildren:
             self.walk_print(child)
                             
    def is_initialized(self):
        return self._isInitialized
            
    def set_attribute(self, attrVal, attrName, enforceConsistant=True, mustExist=True):

        validate_hex(attrVal, attrName)

        if attrName in self._packetAttributes:
            self._packetAttributes[attrName][0].set_data(attrVal, enforceConsistant)
            self._commit_change()
            
        elif mustExist:
            raise AttributeError("Attribute %s not found in packet."%attrName)
            
    def tree_walk(self, funct, aux=None, attribute=None):
        
        if attribute is None:
            attribute = self._root
            
        funct(attribute, aux)
        attributeChildren=attribute.get_children()
        for child in attributeChildren:
             self.tree_walk(funct, aux, child)
    
    def get_time(self):
        
        timeAttr=self._packetAttributes.get('frame.time_epoch')[0]
        
        if timeAttr is None:
            raise AttributeError("frame.time_epoch attribute was not found.")
        
        timeRep=timeAttr.get_fvalue()
        tVals=timeRep.split('.')
        
        if 2 != len(tVals):
            raise AttributeError("AttritbuteError: unexpected time values format. %s"%str(tVals))
        
        upper=int(tVals[0])
        lower=int(tVals[1])
        
        rtn=(upper,lower)
        
        return rtn
    
    def get_num_bytes(self):
        return len(self.get_root().get_data())/2
    
    def get_pkt_data(self):
        return self.get_root().get_data()
    
    def serialize_to_string(self):
        if self._pktTree is None or False == self._isInitialized:
            raise AttributeError("Attempting to serialize uninitialized packet.")
        return self._pktTree.SerializeToString()
    
    #static Packet functions
    @staticmethod
    def parse_from_string(serializedPacketString):
        pktTree=protoShark_pb2.PacketAttributeTree()
        pktTree.ParseFromString(serializedPacketString)
        pkt=None
        if pktTree is not None:
            pkt=Packet(pktTree)
            if pkt is None:
                raise AttributeError("Failed to create packet from PacketAttributeTree.")
        return pkt

#test main
if __name__ == "__main__":
    pass

