#!/usr/bin/python
import random,sys
from decimal import *

def keywordCheck(keyWordList, **kwargs):
    for kw in keyWordList:
        if kw not in kwargs.keys():
            print ("keys:", kwargs.keys())
            raise AttributeError("%s keyword not provided"%kw)
        
def is_ipv4(pkt, **kwargs):
    attrs=pkt.get_attributes()
    if 'ip.version' not in attrs.keys() or 4 != int(attrs['ip.version'][0].get_fvalue()):
        return False
    return True

def is_tcp(pkt, **kwargs):
    attrs=pkt.get_attributes()
    if 'tcp' not in attrs.keys():
        return False
    return True      

def is_server_side(pkt, **kwargs):
    try:
        keywordCheck(['serverPort'], **kwargs)
    except Exception as e:
        raise(e)
    
    serverPort = kwargs['serverPort']
    attrs=pkt.get_attributes()
    srcPort = int(attrs['tcp.srcport'][0].get_fvalue())
    
    if serverPort == srcPort:
        return True
    
    return False

def is_client_side(pkt, **kwargs):
    
    try:
        keywordCheck(['serverPort'])
    except Exception as e:
        raise(e)
    
    if not is_server_side(pkt, serverPort=kwargs['serverPort']):
        return True
    
    return False

def p_select(pA):
    
    if pA < 0 or pA >=1:
        raise ValueError("Input param must be in range (0,1), not inclusive of either 0 or 1.")
    
    #if <= pA, return True. Else, return False.
    p=random.random()
    if p<= pA:
        return True
    return False

#Higher values for 'p' will increase chances of longer sequential runs from given lists
def interleave_pkts(pktListA, pktListB, p=0.5, startA=True, minTimeVariance = .00000000001, maxTimeVariance = .00000000075):
    
    a=None
    b=None
    allPkts=[]
    
    if minTimeVariance < 0 or minTimeVariance > 1:
        raise ValueError("minTimeVariance value must be a floating point number between 0 and 1.")
    
    if maxTimeVariance < 0 or maxTimeVariance > 1:
        raise ValueError("maxTimeVariance value must be a floating point number between 0 and 1.") 
    
    if maxTimeVariance <= minTimeVariance:
        raise ValueError("maxTimeVariance must be greater than minTimeVariance.")
    
    if startA:
        a=pktListA[:]
        b=pktListB[:]
        
    else:
        a=pktListB[:]
        b=pktListA[:]
        
    while len(a) > 0 or len(b) > 0:
        if 0 == len(a):
            for pkt in b:
                allPkts.append(([0,0],pkt))
            del b[:]
            break
        
        if 0 == len(b):
            for pkt in a:
                allPkts.append(([0,0],pkt))
            del a[:]
            break
        
        #append a.next to pkt list   
        if p_select(p):
            pkt = a[0]
            allPkts.append(([0,0],pkt))
            a.remove(pkt)
        
        #append b.next to pkt list
        #swap a and b
        else:
            pkt = b[0]
            allPkts.append(([0,0],pkt))            
            b.remove(pkt)
            c=a
            a=b
            b=c
 
    getcontext().prec =20
    pkt = allPkts[0][1]
    epochTime = pkt.get_attributes()['frame.time_epoch'][0].get_fvalue()
 
    #fix timestamps           
    for entry in allPkts:

        x,y = epochTime.split('.')
     
        entry[0][0]= long(x)&0xffffffff
        entry[0][1]= long(y)&0xffffffff
        
        timeDelta = Decimal(random.uniform(minTimeVariance,maxTimeVariance))
        currentTime = Decimal(epochTime)
        currentTime = (Decimal(1.0)+timeDelta)*currentTime
        epochTime = "%9.9f"%currentTime
            
    return allPkts


#hex to dotted decimal
def hex2dd(hexip):
    a=hexip[:2]
    b=hexip[2:4]
    c=hexip[4:6]
    d=hexip[6:]
    
    val=''
    for each in (a,b,c,d):
        val+=str(int(each,16))
        if each != d:
            val+='.'
        
    return val
        
def sort_into_tcp_sessions(pktList,sessionDict):
    for pkt in pktList:
        attrs=pkt.get_attributes()
        frameProtocols=attrs['frame.protocols'][0]
        pktProtocolList=frameProtocols.get_fvalue().split(':')
        
        #only want tcp packets
        if 'tcp' not in pktProtocolList:
            continue
        
        tcpStream=int(attrs['tcp.stream'][0].get_fvalue())
        if False == sessionDict.has_key(tcpStream):
            sessionDict[tcpStream]={}
            sessionDict[tcpStream]['pkts']=[]
            sessionDict[tcpStream]['clientIp']=None   
            sessionDict[tcpStream]['clientPort']=None   
            sessionDict[tcpStream]['clientSeqnOffset']=None
            sessionDict[tcpStream]['serverSeqnOffset']=None          

        sessionDict[tcpStream]['pkts'].append(pkt)      
        

        