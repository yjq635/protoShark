#!/usr/bin/python


class Transform(object):
    
    def __init__(self, runner, conditionalFunctList=[]):
        
        #transform that will run
        self._runner=runner
        self._conditionalFunct=conditionalFunctList
        
        #if True, tagList will be ignored and transform will always run
        self._always=False
                   
    def run(self, pkt, **kwargs):
        if self._runner is None:
            raise AttributeError("Transform runner not defined.")
        
        #Transform returns True if successful. Returns False on failure. 
        rtn = True
        
        if self.always():
            rtn = self._runner(pkt, **kwargs)
                
        elif self._conditionalFunct is None:
            rtn = self._runner(pkt, **kwargs)
        
        else:
            passed = True
            for f in self._conditionalFunct:
                passed = f(pkt, **kwargs)
                if False == passed:
                    break

            if passed:        
                rtn = self._runner(pkt, **kwargs)   
            
        return rtn
    
    def set_runner(self, runner):
        self._runner=runner
            
    def set_always(self):
        self._always= True
        
    def unset_always(self):
        self._always= False        
        
    def always(self):
        return self._always
    
    
if __name__ == "__main__":
    
    pkt=0
    
    def cond_true(pkt, **kwargs):
        return True
    
    def cond_false(pkt, **kwargs):
        return False
    
    def rt1(pkt, **kwargs):
        return True
    
    def rt2(pkt, **kwargs):
        return True
    
    def rt3(pkt, **kwargs):
        return True
    
    def trans1(pkt, a=None,b=None,vd={},vl=[]):
        for each in (a,b,vd,vl):
            print "Trans1:", str(each)
            
    x="bob"
    y="ted"
    c={"bob":"evil","ted":"good"}
    d=[1,2,3,4]
    
    print("Unconditional call")
    t=Transform(trans1)
    t.run(pkt,a=x,b=y,vd=c,vl=d)
    
    print("Conditional True call")
    t=Transform(trans1,[cond_true])
    t.run(pkt,a=x,b=y,vd=c,vl=d)
    
    print("Conditional Trues with False call")
    t=Transform(trans1,[cond_true,rt1,rt2,cond_false,rt3])
    t.run(pkt,a=x,b=y,vd=c,vl=d)    
    
    print("Conditional False call")
    t=Transform(trans1,[cond_false])    
    t.run(pkt,a=x,b=y,vd=c,vl=d)
    
    print("Conditional False call with always run")
    t=Transform(trans1,[cond_false])
    t.set_always()    
    t.run(pkt,a=x,b=y,vd=c,vl=d)
    
    