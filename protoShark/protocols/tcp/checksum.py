#!/usr/bin/python

import binascii, struct
from protoShark.packets import Packet


def get_pseudoheader(pkt):
    
    attrs = pkt.get_attributes()
    srcaddr=attrs['ip.src'][0].get_data()
    dstaddr=attrs['ip.dst'][0].get_data()
    
    hdr_len=int(attrs['tcp.hdr_len'][0].get_fvalue())
    tcp_len=int(attrs['tcp.len'][0].get_fvalue())
    
    ip_src_addr = srcaddr
    ip_dst_addr = dstaddr
    tcp_hdr_len = hdr_len
    tcp_len =  tcp_len
    total_len = hex((tcp_hdr_len+tcp_len)&0xffff)[2:].zfill(4)
    
    ph=ip_src_addr+ip_dst_addr+"00"+"06"+total_len
    
    return ph


def compute_tcp_checksum(pkt):
    
    attrs=pkt.get_attributes()
    ph=get_pseudoheader(pkt)
    checksumHexOffset = attrs['tcp.checksum'][0].get_hex_offset()
    tcpHexOffset = attrs['tcp'][0].get_hex_offset()

    frame_data = attrs['frame'][0].get_data()
    fd = frame_data[:checksumHexOffset]
    fd += '0000'
    fd += frame_data[checksumHexOffset+4:]
    tcp_seg = fd[tcpHexOffset:]
    tocompute = ph+tcp_seg
        
    while(len(tocompute)%4):
        tocompute+="00"
    
    cList = [tocompute[i:i+4] for i in range(0, len(tocompute), 4)]
        
    num=0
    for n in cList:
        num += (int(n,16))
        num &= 0xffffffff
        
    carry = (num>>16)&0xffff
    num &= 0xffff
    
    num+=carry
    num&=0xffff

    while True:
        carry = (num>>16)&0xffff
        if 0 == carry:
            break

        num+=carry
        num&=0xffff
        
    num^=0xffff        
    checksum= hex(num)[2:].zfill(4)
    pkt.set_attribute(checksum,'tcp.checksum')    
    
        
#TEST main#####TEST main#####TEST maim########TEST main#####TEST main#####TEST main#####TEST maim########TEST main
if __name__=='__main__':
    
    tcp_data = "0b6822b32311a6cb00000000a002aaaa"+"0000"+"00000204ffd70402080adbfa5d270000000001030307"
    data = ""
    ip_src_addr = "ac2df3bc"
    ip_dst_addr = "ac25d6a2"
    tcp_hdr_len = 40 #base 10
    tcp_len = 0
    total_len = hex((tcp_hdr_len+tcp_len)&0xffff)[2:].zfill(4)
    correct_checksum = "4f65"
    
    ph=ip_src_addr+ip_dst_addr+"00"+"06"+total_len
    tocompute=ph+tcp_data+data
    
    while(len(tocompute)%4):
        tocompute+="00"
    
    cList = [tocompute[i:i+4] for i in range(0, len(tocompute), 4)]
    
    print cList
    
    num=0
    for n in cList:
        num += (int(n,16))
        num &= 0xffffffff
        
    carry = (num>>16)&0xffff
    num &= 0xffff
    
    num+=carry
    num&=0xffff

    while True:
        carry = (num>>16)&0xffff
        if 0 == carry:
            break

        num+=carry
        num&=0xffff
        
    num^=0xffff        
    print hex(num)