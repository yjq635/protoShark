#!/usr/bin/python


def ipv4_checksum(pkt):
    
    ip_hex_string=pkt.get_attributes()['ip'][0].get_data()
       
    #add up all 16 bit fields in first 20 bytes of header (not including options)
    sums=0
    for cnt in xrange(0,len(ip_hex_string),4):

        if(20==cnt):
            val=0
        else:
            val=int(ip_hex_string[cnt:cnt+4],16)
        
        sums+=val
    
    #4 most significant bits are carry bits.
    carry=(sums>>16)&0xffff

    #16 least significant bits are sum
    sums&=0xffff

    while(True):
        
        #break when carry bits == 0
        if(0==carry):
            break
        
        #carry gets added to sum
        sums+=carry
        
        #repeat carry/sum addition if carry bits != 0
        carry=(sums>>16)&0xffff
        sums&=0xffff
    
    #flip all bits in sum to get checksum, convert to hex string, and pad as needed
    chksum=hex(0xffff^sums)[2:].zfill(4)
    
    pkt.set_attribute(chksum,'ip.checksum')


    

#TEST main#####TEST main#####TEST maim########TEST main#####TEST main#####TEST main#####TEST maim########TEST main
if __name__=='__main__':
    
    pass
    #pass hex string representation of ip header bytes
    #chksum=ipv4_checksum("4500003cac7f40004006903a7f000001ac29c163")
    
    #if('b9e6' != chksum):
    #    raise RuntimeError("Failed to correctly calculate ip header")
    
    #print chksum
    
    