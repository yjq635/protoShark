# Build ProtoShark Docker Image
From the directory containing the Dockerfile, run the following command:<br> 
`docker build -t registry.gitlab.com/mlandriscina/protoshark .`<br>

*<b>NOTE: It probably will take a long time to build this image. Luckily, you only have to do this once.</b>*

# OR ... Pull Pre-built Docker Image
`docker pull registry.gitlab.com/mlandriscina/protoshark`

# Useful Commands
*mount host directory c:\Users\Public\protoShark to /devProtoShark in container. Drops to user python prompt by default.*<br>
`docker run -it -v c:/Users/Public/protoShark:/devProtoShark registry.gitlab.com/mlandriscina/protoshark`

*same as above but drops user to bash prompt*<br>
`docker run -it -v c:/Users/Public/protoShark:/devProtoShark registry.gitlab.com/mlandriscina/protoshark /bin/bash`

*same as above but executes Python program file 'doThis.py' inside Docker container*<br>
*quick way of running a linux python program from a Windows command line (assuming container is linux-based)<br>*
`docker run -it -v c:/Users/Public/protoShark:/devProtoShark registry.gitlab.com/mlandriscina/protoshark /protoShark/projects/doThis.py`

*<b>run container that will listen on, write to, or otherwise need to access network interface(s)</b>*<br>
`docker run --privileged -it -v c:/Users/Public/protoShark:/devProtoShark registry.gitlab.com/mlandriscina/protoshark /bin/bash`

*update to latest version of protoShark before dropping user to bash prompt*<br>
`docker run -it -v c:/Users/Public/protoShark:/devProtoShark registry.gitlab.com/mlandriscina/protoshark /home/user/update.sh`

*remove all docker containers via Windows command line*<br>
for /f %x in ('docker ps -aq') do docker rm %x

*remove all docker images via Windows command line*<br>
for /f %x in ('docker images -aq') do docker rmi %x